# War Card Game

This is a simple Swift program I made for the Mobile Application Programming class as an assignment, where you play a game of War in a command line. In this game, each player is given 26 cards from a standard deck of shuffled cards. Each player plays a card from their hand and the player with the highest ranking card gets to take both of the cards. If there is a tie, then each player plays three cards face down, with another card face up. The player with the highest ranking card takes all of the played cards. If there is another tie, then the process is repeated until there is a winner.

Players continue playing cards until a player runs out of cards, which can happen at any point in the game. The player who still has cards wins the game.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes.

### Prerequisites
- **Swift**: The programming language used to build this game. Can be downloaded from https://swift.org/. For instructions on how to install Swift, see https://swift.org/getting-started/.
- **XCode (Optional)**: If you are using a Mac, you can download XCode from the App Store (https://developer.apple.com/xcode/) and open the project with XCode.

### Installing

Ensure that you have installed Swift and (optionally) XCode onto your machine. Once installed, you can run the software using one of two methods:

- **Without XCode**: Run the "sw_run" bash script located in the root of the project folder (you may need to give yourself "execute" permissions to the file with the **chmod** command).
- **With XCode**: You can open the project by clicking "File > Open" in the Menu bar and clicking on the "swift_assignment.xcodeproj" file. Then you can click the Build and Run button to run the project.

### Usage

Upon running the program, you will be asked to enter your name. Once you have entered your name, you will begin the game. In this game, you will be competing against a computer player named "Hal." The input in this game is very minimal: you just need to press "Enter" whenever you're asked to do so. The game will automatically choose the first card in your hand to play whenever you are required to play a card. The card you and the computer player played will be shown on the screen, as well as who won the round. The cards that are played face-down during a tie will just be shown as "Each player played a card facedown."

The program ends when a player runs out of cards, and the winner of the game will be shown onscreen.

## Built With

* [Swift](https://swift.org/) - The programming language used to build this game.
* [XCode](https://developer.apple.com/xcode/) - The IDE used to make the game.

## Authors

* **Corey Janzen**  - [cjanzen](https://bitbucket.org/cjanzen/)


> Written with [StackEdit](https://stackedit.io/).
