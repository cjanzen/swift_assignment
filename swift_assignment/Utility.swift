//
//  Utility.swift
//  swift_assignment
//
//  Created by Corey Janzen (cst219) on 2018-03-22.
//  Copyright © 2018 cst219. All rights reserved.
//
// Description: Contains miscellaneous resources/functions that don't
//              Fit into a specific class

import Foundation

/** Contains all the ranks a card can have */
public enum Rank : Int
{
    case two = 0, three, four, five, six, seven, eight, nine, ten,
        jack, queen, king, ace;
    
    /** Property that creates an array of all ranks */
    static var all : [Rank]
    {
        return [.two, .three, .four, .five, .six, .seven, .eight, .nine, .ten,
                .jack, .queen, .king, .ace];
    }

}

/**
 An array of rank names as strings. Used in the Cards struct to print
 out its description as a string.
*/
var rankNames : [String] = ["Two", "Three", "Four", "Five", "Six", "Seven", "Eight",
                            "Nine", "Ten", "Jack", "Queen", "King", "Ace"];

/** Contains all the ranks a card can have */
public enum Suit : Int
{
    case hearts = 0, spades, diamonds, clubs;
    
    /** Property that creates an array of all suits */
    static var all : [Suit]
    {
        return [.hearts, .spades, .diamonds, .clubs];
    }
}

/**
 An array of suit names as strings. Used in the Cards struct to print
 out its description as a string.
*/
var suitNames : [String] = ["♥︎", "♠︎", "♦︎", "♣︎"];

/**
 Returns the name of the player from the command line
 
 - Author:
 Corey Janzen (cst219)
 
 - returns:
 A string containing the player's name
 */
public func getName() -> String
{
    var name: String?; // The name we will be returning
    
    // Ask user for input
    print("Please enter your name");
    
    // While we haven't received input yet
    while (name == nil || name! == "")
    {
        // Scan input
        name = readLine();
        
        // If no input was received, display error message
        if (name == nil || name! == "")
        {
            print("You must enter a name");
        }
    }
    
    // Return the name
    return name!;
}

/**
 Compares card1 with card2 to see which card is greater.
 
 - returns:
 true if card2 is greater than card1
 false otherwise
 */
public func < (card1: Card, card2: Card) -> Bool
{
    return card1.rank.rawValue < card2.rank.rawValue;
}

/**
 Gets a random number using OS-specific behaviour, since arc4random
 doesn't work on linux.
 
 - returns:
 A random int that's no higher than the max specified
*/
public func randomInt(max: UInt32) -> Int
{
	#if os(Linux)
    	return random() % Int(max);
	#else
		return Int(arc4random_uniform(max));
	#endif
}
