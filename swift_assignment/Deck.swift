//
//  Deck.swift
//  swift_assignment
//
//  Created by Corey Janzen (cst219) on 2018-03-23.
//  Copyright © 2018 cst219. All rights reserved.
//

import Foundation

/**
 The deck class represents a full deck of 52 cards.
 This class has one property - an array of Card objects.
 It has methods for initializing the deck (creates 52
 cards and shuffles them), shuffling the deck of cards,
 and getting the next card from the deck.
 
 - Author:
 Corey Janzen (cst219)
 */
public class Deck
{
    /**
     Constant for determining how many times we should swap 2 cards in a shuffle
     */
    private let SHUFFLE_LOOP = 1000;
    
    /** The actual "deck" of cards */
    private var cards : [Card];
    
    /**
     Initializes the Deck with shuffled 52 cards.
     */
    public init()
    {
        // Initialize cards array
        self.cards = [Card]();
        
        // Create all cards for the deck
        for r in Rank.all
        {
            for s in Suit.all
            {
                let card : Card = Card(rank: r, suit: s);
                cards += [card];
            }
        }
        
        // Shuffle the cards
        shuffle();
    }
    
    /** Shuffles deck so the cards are in a random order. */
    public func shuffle()
    {
        // Swap two cards for the number of times specified by SHUFFLE_LOOP
        for _ in 1...SHUFFLE_LOOP
        {
            // Pick two cards to swap
            let i = randomInt(max: UInt32(cards.count));
            let j = randomInt(max: UInt32(cards.count));
            
            // If the cards aren't the same
            if i != j
            {
                // Store the card in i temporarily
                let temp = cards[i];
                
                // swap card i with card j
                cards[i] = cards[j];
                
                // Swap card j with temporary card
                cards[j] = temp;
            }
        }
    }
    
    /**
     Retrieves a card from the top of the deck (or nil if no cards exist in the deck).
     
     - returns:
     Card?
     */
    public func getNextCard() -> Card?
    {
        return cards.popLast();
    }
}
