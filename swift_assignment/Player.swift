//
//  Player.swift
//  swift_assignment
//
//  Created by Corey Janzen (cst219) on 2018-03-23.
//  Copyright © 2018 cst219. All rights reserved.
//

import Foundation

/**
 This class represents one single player. A player has a
 name and a Hand object. The computed property description
 should return a String of the form "Jason has 41 cards",
 where Jason is the name of the player and 41 is the count
 of cards from the player's hand.
 
 - Author:
 Corey Janzen (cst219)
 */
public class Player : CustomStringConvertible
{
    /** The player's hand */
    private var hand : Hand;
    
    /** The player's name */
    private var name : String;
    
    /** The number of cards in the player's hand */
    public var count : Int
    {
        return hand.count;
    }
    
    /**
     Gets the string representation of this player,
     showing their name and the number of cards they have
     */
    public var description: String
    {
        get
        {
            return name + " has \(count) " + (count > 1 ? "cards" : "card");
        }
    }
    
    /** Creates a Player object with the given name and hand */
    public init(name : String, hand : Hand)
    {
        self.name = name;
        self.hand = hand;
    }
	
	/** Creates a Player object with the given name and an empty hand */
    public convenience init(name : String)
    {
        self.init(name: name, hand: Hand(cards: [Card]()));
    }
    
    /**
     Gets the name of the player
     
     - returns:
     String
     */
    public func getName() -> String
    {
        return name;
    }
    
    /**
     Gets the next card from the player's hand. If there are no
     cards left, nil is returned instead.
     
     - returns:
     Card?
     */
    public func getNextCard() -> Card?
    {
        return hand.getNextCard();
    }
    
    /**
     Adds the given card to the player's discard pile
     */
    public func addCard(card : Card)
    {
        hand.addToDiscard(card: card);
    }
}
