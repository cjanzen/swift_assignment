//
//  Hand.swift
//  swift_assignment
//
//  Created by Corey Janzen (cst219) on 2018-03-23.
//  Copyright © 2018 cst219. All rights reserved.
//

import Foundation

/**
 This class will hold all the cards in a specific player's hand.
 The player's hand will consist of cards to be played and collected
 discarded cards. If the cards array is empty, then all elements
 in the discard are added to cards array. The player is out of cards
 when both the cards and discard arrays are empty.
 The computed property count will be the sum of the count elements in
 both cards and discard arrays.
 
 - Author:
 Corey Janzen (cst219)
 */
public class Hand
{
    /** Player's cards in hand */
    private var cards : [Card];
    
    /** Player's discard pile */
    private var discard : [Card];
    
    /** Counts the number of cards both in hand and in discard */
    public var count : Int
    {
        return cards.count + discard.count;
    }
    
    /**
     Takes a card from the player's hand. If the hand is empty,
     then all the cards are taken from discard and are put into
     the hand and one of those cards will be returned. If both
     are empty, then nil is returned.
     
     - returns:
     Card?
     */
    public func getNextCard() -> Card?
    {
        // The card that will be returned
        var card : Card?;
        
        // Condition 1: Cards are in hand
        if !cards.isEmpty
        {
            // Take the first card from hand and return it
            card = cards.remove(at: 0);
        }
        // Condition 2: Hand is empty, but discard is not
        else if !discard.isEmpty
        {
            // Move all cards from discard to hand
            cards += discard;
            discard.removeAll();
            
            // Take the first card from hand and return it
            card = cards.remove(at: 0);
        }
        // Condition 3: Both hand and discard are empty
        // Do nothing, just return nil.
        
        return card;
    }
    
    /** Adds the given card to the discard pile */
    public func addToDiscard(card: Card)
    {
        discard.append(card);
    }
    
    /**
     Creates a hand with the given cards.
     Also creates an empty discard pile
     */
    public init(cards: [Card])
    {
        // Create the hand
        self.cards = cards;
        
        // Create the discard pile
        self.discard = [Card]();
    }
}
