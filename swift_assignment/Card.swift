//
//  Card.swift
//  swift_assignment
//
//  Created by Corey Janzen (cst219) on 2018-03-22.
//  Copyright © 2018 cst219. All rights reserved.
//

/**
 This struct represents a single card in the game of war.
 It contains the rank and the suit of the card, as well
 as the description method to print out the card details
 into a string.

 - Author:
 Corey Janzen (cst219)
*/
public struct Card
{
	/** The rank of this card as a Rank enum type */
	public var rank : Rank;
	/** The suit of this card as a Suit enum type */
	public var suit : Suit;
	
	/**
	 Returns a string description of this card formatted like this:
	 "Rank of Suit"
	*/
	public var description : String
	{
		return rankNames[rank.rawValue] + " of " + suitNames[suit.rawValue];
	}
    
    public init(rank: Rank, suit: Suit)
    {
        self.rank = rank;
        self.suit = suit;
    }
}
