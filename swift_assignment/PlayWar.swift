//
//  PlayWar.swift
//  swift_assignment
//
//  Created by Corey Janzen (cst219) on 2018-03-23.
//  Copyright © 2018 cst219. All rights reserved.
//

import Foundation

/**
 This is the class that handles the playing of the game.
 It has two properties: the Player object for the human
 player and a Player object for the computer player.

 Contains methods necessary to play the game, including
 playWar, playRound, dealCards, and checkForWinner.
 
 - Author:
 Corey Janzen (cst219)
 */
public class PlayWar
{
    /** The default name for he computer player */
	let DEFAULT_NAME = "Hal";
	
	/** The human player */
	private var human : Player;
	
	/** The computer player */
	private var computer : Player;
	
	/**
	 Initializes a PlayWar object to play the game of war.
	 It asks the user for their name and creates a Player
	 object for the human property using the provided name.
	 It also creates a Player object for the computer property
	 using a default computer name.
	 */
	public init()
	{
		// Display Intro message
		print("Welcome to Command Line War!");
		
		// Create the human player with the given name
		self.human = Player(name : getName());
		// Create the computer player with the default name
		self.computer = Player(name : DEFAULT_NAME);
	}
	
	/**
	 Takes in two cards, first one is for human and second for the
	 computer plauer. This method should determine if one player won -
	 a player loses if their card is nil.
	
	 - returns:
	 Boolean indicating if there was a winner or not.
	 */
	public func checkForWinner(cardPlayer1: Card?, cardPlayer2: Card?) -> Bool
	{
		return cardPlayer1 == nil || cardPlayer2 == nil;
	}
	
	/**
	 This method instantiates a Deck object. Then using that Deck
	 object deal 26 cards to each player
	 */
	public func dealCards()
	{
		// An indicator that will be used to divide cards evenly
		var i = true;
		
		// Create the deck with 52 cards
		let deck = Deck();
		
		// Take the first card out
		var card = deck.getNextCard();
		
		// Deal 26 cards to each player
		while (card != nil)
		{
			// Take turns giving each player a card
			if (i)
			{
				human.addCard(card: card!);
				i = false;
			}
			else
			{
				computer.addCard(card: card!);
				i = true;
			}
			
			// Take the next card out of the deck
			card = deck.getNextCard();
		}
	}
	
	/**
	 Plays a single round of War, which goes like this:
	 	- Each player plays a card
		- If there's no winner yet
			- If there's no tie, the round winner takes all played cards
			- If there's a tie, the tie logic runs (see playTie)
		- Return boolean indicating if someone won yet.
	 
	 - returns:
	 Boolean
	 */
	public func playRound() -> Bool
	{
		// Indicator to tell if anyone has won the game yet
		var hasWon : Bool = false;
		
		// Each player plays a card
		let humanCard = human.getNextCard();
		let computerCard = computer.getNextCard();
		
		// Check if there is a winner yet
		hasWon = checkForWinner(
			cardPlayer1: humanCard,
			cardPlayer2: computerCard
		);
		
		// If there is no winner for the game
		if (!hasWon)
		{
			// Print each player's played card
			print("\n\(human.getName()) played the \(humanCard!.description)");
			print("\n\(computer.getName()) played the \(computerCard!.description)");
			
			// Condition 1: Human wins round
			if (computerCard! < humanCard!)
			{
				// Human gets computer's card
				human.addCard(card: computerCard!);
				human.addCard(card: humanCard!);
				print("\n\(human.getName()) wins this round");
			}
			// Condition 2: Computer wins round
			else if (humanCard! < computerCard!)
			{
				// Computer gets human's card
				computer.addCard(card: humanCard!);
				computer.addCard(card: computerCard!);
				print("\n\(computer.getName()) wins this round");
			}
			// Condition 3: Tie
			else
			{
				// Run tie logic
				hasWon = playTie(
					humanCard: humanCard,
					computerCard: computerCard
				);
			}
		}	
		
		// Return the winner indicator for the game
		return hasWon;
	}
	
	/**
	 Handles the logic for when there is a tie between two players.
	 When there is a tie:
		1. Each player plays 3 cards face-down
		2. Each player plays 1 card face-up
		3. If there is a tie between the face up cards,
		   repeat steps 1-2 until there is a higher ranking
	       between the two).
		4. The player with the higher ranking wins all played cards
	 
	 - returns:
	 Boolean
	 */
	public func playTie(humanCard: Card?, computerCard: Card?) -> Bool
	{
		// Counter for the loop
		var i = 0;
		// Indicator to tell if anyone has won the game yet
		var hasWonGame : Bool = false;
		// Indicator to tell if anyone has won the tie yet
		var hasWonTie : Bool = false;
		// Human player's stack of played cards
		var humanCards : [Card?] = [humanCard];
		// Computer player's stack of played cards
		var computerCards : [Card?] = [computerCard];
		
		// While no one has broken the tie yet or won the game yet
		while !hasWonTie && !hasWonGame
		{
			// Reset counter to 0
			i = 0;
			// Display tie message
			print("\nTie!! Draw 3 cards facedown, then 1 up");
			// Play 4 cards (3 face-down, 1 face-up) for each player
			while i < 4 && !hasWonGame
			{
				humanCards += [human.getNextCard()];
				computerCards += [computer.getNextCard()];
				
				// Check for a game winner
				hasWonGame = checkForWinner(
					cardPlayer1: humanCards.last!,
					cardPlayer2: computerCards.last!
				);
				
				// If this is not the face-up card and each player
				// played a card, display a message
				if (i != 3 && !hasWonGame)
				{
					print("Each player played a card facedown");
				}
				
				// Increment counter
				i += 1;
			}
			
			// If there is no winner yet
			if (!hasWonGame)
			{
				// Display each player's face-up card
				print("\n\(human.getName()) played the \(humanCards.last!!.description)");
				print("\n\(computer.getName()) played the \(computerCards.last!!.description)");
				
				// Condition 1: Human wins round
				if (computerCards.last!! < humanCards.last!!)
				{
					// Human gets all of computer's played cards
					for card in computerCards
					{
						human.addCard(card: card!);
					}
					for card in humanCards
					{
						human.addCard(card: card!);
					}
					// Indicate that the tie was broken
					hasWonTie = true;
					print("\n\(human.getName()) wins this round");
				}
				// Condition 2: Computer wins round
				else if (humanCards.last!! < computerCards.last!!)
				{
					// Computer gets all of human's played cards
					for card in humanCards
					{
						computer.addCard(card: card!);
					}
					for card in computerCards
					{
						computer.addCard(card: card!);
					}
					// Indicate that the tie was broken
					hasWonTie = true;
					print("\n\(computer.getName()) wins this round");
				}
				// Condition 3: Tie
				// Do nothing, since we want to our logic to loop
				// again to break the tie.
			}
		}
		
		// Return the winner indicator for the game
		return hasWonGame;
	}
	
	/**
	 This method calls the dealCards method and then loops while
	 the game is still on. At the start of each round display the
	 current status of each player by displaying the description
	 property for each player. Then call the playRound method to
	 play a round.
	 */
	public func playWar()
	{
		// Deal 26 cards to each player
		dealCards();
		
		// For each round we play until there is a winner
		repeat
		{
			// Display stats
			print("*******************************");
			print(human);
			print(computer);
			print("*******************************");
			
			// Wait for user's input
			print("Please press enter to continue.");
			readLine();
			
		// Call playRound to play the round and check for winner
		} while (!playRound()); 
		
		// Print out the appropriate message based on who won
		// Condition 1: Human lost
		if (human.count == 0 && computer.count > 0)
		{
			print(computer.getName() + " is the winner!!!");
		}
		// Condition 2: Computer lost
		else if (computer.count == 0 && human.count > 0)
		{
			print(human.getName() + " is the winner!!!");
		}
		// Condition 3: There's a tie
		else
		{
			print("There's a tie!");
		}
	}
}
