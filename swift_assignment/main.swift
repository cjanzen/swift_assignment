//
//  main.swift
//  swift_assignment
//
//  Created by Corey Janzen (cst219) on 2018-03-22.
//  Copyright © 2018 cst219. All rights reserved.
//

import Foundation

/* This was included to get randomization to work on Linux */
#if os(Linux)
    srandom(UInt32(time(nil)))
#endif

// Create a PlayWar object and play the game!
let war = PlayWar();
war.playWar();

// All of this is test code. If you want to test the code, just uncomment
// and make sure that human and computer are public in PlayWar
/*
func testUtilities()
{
    // Test that getName works
    let name = getName();
    print("Name is " + name);
    
    // Test that you can print enums
    for r in Rank.all
    {
        print(rankNames[r.rawValue]);
    }
    
    for s in Suit.all
    {
        print(suitNames[s.rawValue]);
    }
    
    // Test that the overloaded < function works as intended
    let cards : [Card] = [
        Card(rank: Rank.four, suit: Suit.clubs),
        Card(rank: Rank.five, suit: Suit.diamonds),
        Card(rank: Rank.four, suit: Suit.hearts)
    ];
    
    print("Card 0 vs Card 1 (should be true): \(cards[0] < cards[1])");
    print("Card 1 vs Card 0 (should be false): \(cards[1] < cards[0])");
    print("Card 0 vs Card 2 (should be false): \(cards[0] < cards[2])");
}

func testCard()
{
    // Test that description works as intended
    for r in Rank.all
    {
        for s in Suit.all
        {
            let card : Card = Card(rank: r, suit: s);
            print(card.description);
        }
    }
    
}

func testDeck()
{
    // Test that the deck functions as intended
    let deck = Deck();
    var card = deck.getNextCard();
    
    while (card != nil)
    {
        print(card!.description);
        card = deck.getNextCard();
    }
}

func testHand()
{
    // Create a Hand object for testing
    var hand = Hand(cards: [Card]());
    
    // Test that addToDiscard works (and Condition 2 works on getNextCard)
    hand.addToDiscard(card: Card(rank: Rank.ace, suit: Suit.clubs));
    print(hand.getNextCard()!.description);
    
    // Create some new card objects with some actual Cards
    let cards : [Card] = [
        Card(rank: Rank.eight, suit: Suit.diamonds),
        Card(rank: Rank.five, suit: Suit.hearts),
        Card(rank: Rank.four, suit: Suit.spades),
        Card(rank: Rank.jack, suit: Suit.clubs)
    ];
    hand = Hand(cards: cards);
    
    // Test that getNextCard works (and Conditions 1 and 3 work)
    var card = hand.getNextCard();
    var i = 1;
    while (card != nil)
    {
        print("Card \(i) = " + card!.description);
        i += 1;
        card = hand.getNextCard();
    }
    
    // Test that count works (should be 5 cards)
    hand = Hand(cards: cards);
    hand.addToDiscard(card: Card(rank: Rank.ace, suit: Suit.clubs));
    if hand.count == 5
    {
        print("Count works as expected!");
    }
    else
    {
        print("Failed asserting that \(hand.count) = 5");
    }
    
    // Remove a card to make sure count works
    hand.getNextCard();
    if hand.count == 4
    {
        print("Count works as expected!");
    }
    else
    {
        print("Failed asserting that \(hand.count) = 4");
    }
}

func testPlayer()
{
    // Create player object
    let name = "Test";
    let hand = Hand(cards: [Card]());
    let player = Player(name: name, hand: hand);
    
    // Test that getName works
    if player.getName() == name
    {
        print("getName() works as expected!");
    }
    else
    {
        print("Failed asserting \(player.getName()) = \(name)");
    }
    
    // Test that addCard and getNextCard works as intended
    player.addCard(card: Card(rank: Rank.ace, suit: Suit.clubs));
    print(player.getNextCard()!.description);
    
    // Test that count works as intended
    player.addCard(card: Card(rank: Rank.ace, suit: Suit.clubs));
    if player.count == 1
    {
        print("Count works as expected!");
    }
    else
    {
        print("Failed asserting that \(player.count) = 1");
    }
    
    // Test that description works as intended
    print(player);
    player.addCard(card: Card(rank: Rank.ace, suit: Suit.clubs));
    print(player);
}

func testPlayWar()
{    
    print("Testing checkForWinner and dealCards:");
    // Create a PlayWar object
    let war = PlayWar();
    
    // Test that checkForWinner works as intended
    let card1 = Card(rank: Rank.ace, suit: Suit.clubs)
    print("card1 vs. card1 (should be false): \(war.checkForWinner(cardPlayer1: card1, cardPlayer2: card1))");
    print("card1 vs. nil (should be true): \(war.checkForWinner(cardPlayer1: card1, cardPlayer2: nil))");
    print("nil vs. card1 (should be true): \(war.checkForWinner(cardPlayer1: nil, cardPlayer2: card1))");
    print("nil vs. nil (should be true): \(war.checkForWinner(cardPlayer1: nil, cardPlayer2: nil))");
    
    // Test that dealCards works as intended
    // (need to make human and computer public for this to work)
    war.dealCards();
    print(war.human);
    print(war.computer);
}

func testPlayWarTieHumanWins()
{
    print("testPlayWarTieHumanWins:");
    // Test data
    let hCards = [
        Card(rank: Rank.eight, suit: Suit.hearts),
        Card(rank: Rank.seven, suit: Suit.hearts),
        Card(rank: Rank.seven, suit: Suit.hearts),
        Card(rank: Rank.ace, suit: Suit.hearts),
    ];
    
    let cCards = [
        Card(rank: Rank.eight, suit: Suit.hearts),
        Card(rank: Rank.seven, suit: Suit.hearts),
        Card(rank: Rank.seven, suit: Suit.hearts),
        Card(rank: Rank.two, suit: Suit.hearts),
    ];
    
    let card1 = Card(rank: Rank.ace, suit: Suit.clubs);
    
    // Create PlayWar object
    let war = PlayWar();
    
    // Give human and computer test cards
    for card in hCards
    {
        war.human.addCard(card: card);
    }
    
    for card in cCards
    {
        war.computer.addCard(card: card);
    }
    
    // Test that a human can win in a tie
    print("The following should return false: \(war.playTie(humanCard: card1, computerCard: card1))");
    
    print(war.human);
    print(war.computer);
}

func testPlayWarTieComputerWins()
{
    print("testPlayWarTieComputerWins:");
    // Test data
    let cCards = [
        Card(rank: Rank.eight, suit: Suit.hearts),
        Card(rank: Rank.seven, suit: Suit.hearts),
        Card(rank: Rank.seven, suit: Suit.hearts),
        Card(rank: Rank.ace, suit: Suit.hearts),
        Card(rank: Rank.eight, suit: Suit.hearts),
        Card(rank: Rank.seven, suit: Suit.hearts),
        Card(rank: Rank.seven, suit: Suit.hearts),
        Card(rank: Rank.ace, suit: Suit.hearts),
    ];
    
    let hCards = [
        Card(rank: Rank.eight, suit: Suit.hearts),
        Card(rank: Rank.seven, suit: Suit.hearts),
        Card(rank: Rank.seven, suit: Suit.hearts),
        Card(rank: Rank.ace, suit: Suit.hearts),
        Card(rank: Rank.eight, suit: Suit.hearts),
        Card(rank: Rank.seven, suit: Suit.hearts),
        Card(rank: Rank.seven, suit: Suit.hearts),
        Card(rank: Rank.two, suit: Suit.hearts),
    ];
    
    let card1 = Card(rank: Rank.ace, suit: Suit.clubs);
    
    // Create PlayWar object
    let war = PlayWar();
    
    // Give human and computer test cards
    for card in hCards
    {
        war.human.addCard(card: card);
    }
    
    for card in cCards
    {
        war.computer.addCard(card: card);
    }
    
    // Test that a Computer can win in a tie
    print("The following should return false: \(war.playTie(humanCard: card1, computerCard: card1))");
    print(war.human);
    print(war.computer);
}

func testPlayWarTieEndsPrematurely()
{
    print("testPlayWarTieEndsPrematurely:");
    // Test data
    let hCards = [
        Card(rank: Rank.eight, suit: Suit.hearts),
        Card(rank: Rank.seven, suit: Suit.hearts),
        Card(rank: Rank.seven, suit: Suit.hearts),
        Card(rank: Rank.ace, suit: Suit.hearts),
    ];
    
    let cCards = [
        Card(rank: Rank.eight, suit: Suit.hearts),
        Card(rank: Rank.seven, suit: Suit.hearts),
        Card(rank: Rank.seven, suit: Suit.hearts),
    ];
    
    let card1 = Card(rank: Rank.ace, suit: Suit.clubs);
    
    // Create PlayWar object
    let war = PlayWar();
    
    // Give human and computer test cards
    for card in hCards
    {
        war.human.addCard(card: card);
    }
    
    for card in cCards
    {
        war.computer.addCard(card: card);
    }
    
    // Test that a computer can win in a tie
    print("The following should return true: \(war.playTie(humanCard: card1, computerCard: card1))");
}

func testPlayWarRound()
{
    print("testPlayWarRound:");
    // Test data
    let hCards = [
        // Human wins
        Card(rank: Rank.ace, suit: Suit.hearts),
        // Computer wins
        Card(rank: Rank.two, suit: Suit.hearts),
        // Tie
        Card(rank: Rank.two, suit: Suit.hearts),
        Card(rank: Rank.ace, suit: Suit.hearts),
        Card(rank: Rank.ace, suit: Suit.hearts),
        Card(rank: Rank.ace, suit: Suit.hearts),
        Card(rank: Rank.ace, suit: Suit.hearts),
        // Human wins game
        Card(rank: Rank.ace, suit: Suit.hearts),
        Card(rank: Rank.ace, suit: Suit.hearts),
    ];
    
    let cCards = [
        // Human wins
        Card(rank: Rank.eight, suit: Suit.hearts),
        // Computer wins
        Card(rank: Rank.ace, suit: Suit.hearts),
        // Tie
        Card(rank: Rank.two, suit: Suit.hearts),
        Card(rank: Rank.two, suit: Suit.hearts),
        Card(rank: Rank.two, suit: Suit.hearts),
        Card(rank: Rank.two, suit: Suit.hearts),
        Card(rank: Rank.two, suit: Suit.hearts),
    ];
    
    // Create PlayWar object
    let war = PlayWar();
    
    // Give human and computer test cards
    for card in hCards
    {
        war.human.addCard(card: card);
    }
    
    for card in cCards
    {
        war.computer.addCard(card: card);
    }
    
    // Test playRound for each possible condition
    print("Return false: \(war.playRound())");
    print("Return false: \(war.playRound())");
    print("Return false: \(war.playRound())");
    print("Return false: \(war.playRound())");
    print("Return true: \(war.playRound())");
    
    print(war.human);
    print(war.computer);
}

//testUtilities();
//testCard();
//testDeck();
//testHand();
//testPlayer();
testPlayWar();
testPlayWarTieHumanWins();
testPlayWarTieComputerWins();
testPlayWarTieEndsPrematurely();
testPlayWarRound();
*/
