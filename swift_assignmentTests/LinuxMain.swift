import XCTest
@testable import swift_assignmentTests

XCTMain([
    testCase(swift_assignmentTests.allTests),
])
